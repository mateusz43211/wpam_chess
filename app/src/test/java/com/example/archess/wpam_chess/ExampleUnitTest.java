package com.example.archess.wpam_chess;

import com.example.archess.wpam_chess.Logic.MoveValidator;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void moveValidation_isCorrect() {
        String fen1 = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
        String fen2 = "rnbqkbnr/pppppppp/8/8/8/P7/1PPPPPPP/RNBQKBNR b KQkq - 0 1";
        ArrayList<String> result = MoveValidator.getMoveFromStatus(fen1, fen2);

        assertEquals("a2", result.get(0));
        assertEquals("a3", result.get(1));

        fen1 = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
        fen2 = "rnbqkbnr/pppppppp/8/8/8/P7/PPPPPPPP/R1BQKBNR b KQkq - 0 1";
        result = MoveValidator.getMoveFromStatus(fen1, fen2);

        assertEquals("b1", result.get(0));
        assertEquals("a3", result.get(1));

        fen1 = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
        fen2 = "rnbqkbnr/pppppppp/8/8/8/P7/PPPPPPPP/R1BQKBNR b KQkq - 0 1";
        result = MoveValidator.getMoveFromStatus(fen1, fen2);

        assertEquals("b1", result.get(0));
        assertEquals("a3", result.get(1));
    }
}