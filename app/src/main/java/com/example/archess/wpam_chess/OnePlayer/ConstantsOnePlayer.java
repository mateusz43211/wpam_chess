package com.example.archess.wpam_chess.OnePlayer;

import java.util.HashMap;
import java.util.Map;

class ConstantsOnePlayer {

    class ServiceStatus {
        static final int RUNNING = 1;
        static final int NEW_GAME_FINISHED = 2;
        static final int LIST_POSSIBLE_MOVES_FINISHED = 3;
        static final int MOVE_FIGURE_PLAYER_FINISHED = 4;
        static final int MOVE_FIGURE_AI_FINISHED = 5;
        static final int CHECK_GAME_OVER_FINISHED = 6;
        static final int PLAYER_HELP_FINISHED = 7;
        static final int GET_TURN_FINISHED = 8;
        static final int UNDO_MOVE_FINISHED = 9;
        static final int RESET_BOARD_FINISHED = 10;
        static final int ERROR = 11;
    }

    static final String NEW_GAME = "NEW_GAME";
    static final String LIST_POSSIBLE_MOVES = "LIST_POSSIBLE_MOVES";
    static final String MOVE_FIGURE_PLAYER = "MOVE_FIGURE_PLAYER";
    static final String MOVE_FIGURE_AI = "MOVE_FIGURE_AI";
    static final String CHECK_GAME_OVER = "CHECK_GAME_OVER";
    static final String PLAYER_HELP = "PLAYER_HELP";
    static final String GET_TURN = "GET_TURN";
    static final String UNDO_MOVE = "UNDO_MOVE";
    static final String RESET_BOARD = "RESET_BOARD";

    static Map<String, String> commandURLMap = new HashMap<>();

    static {
        // new game
        String path = "http://chess-api-chess.herokuapp.com/api/v1/chess/one";
        commandURLMap.put(NEW_GAME, path);

        // list possible moves
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/one/moves";
        commandURLMap.put(LIST_POSSIBLE_MOVES, path);

        // move figure player
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/one/move/player";
        commandURLMap.put(MOVE_FIGURE_PLAYER, path);

        // move figure AI
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/one/move/ai";
        commandURLMap.put(MOVE_FIGURE_AI, path);

        // check game over
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/one/check";
        commandURLMap.put(CHECK_GAME_OVER, path);

        // player help
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/one/help";
        commandURLMap.put(PLAYER_HELP, path);

        // get turn
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/one/turn";
        commandURLMap.put(GET_TURN, path);

        // undo move
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/one/undo";
        commandURLMap.put(UNDO_MOVE, path);

        // reset board
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/one/undo";
        commandURLMap.put(UNDO_MOVE, path);

    }


}
