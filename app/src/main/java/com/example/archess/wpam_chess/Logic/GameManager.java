package com.example.archess.wpam_chess.Logic;

import android.content.Context;
import android.os.Handler;

import com.example.archess.wpam_chess.MyResultReceiver;
import com.example.archess.wpam_chess.OnGameListener;

public abstract class GameManager implements MyResultReceiver.Receiver, IGameManager {
    protected final Context context;
    protected final MyResultReceiver myResultReceiver;

    protected String GAME_ID;
    protected OnGameListener onGameListener;

    public GameManager(Context context){
        this.context = context;

        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);
    }

    public void onPause(){
        myResultReceiver.setReceiver(null);
    }

    public void onResume(){
        myResultReceiver.setReceiver(this);
    }

    public void setOnGameListener(OnGameListener listener){
        this.onGameListener = listener;
    }

    public abstract void start();
    public abstract void checkGameOver();
}
