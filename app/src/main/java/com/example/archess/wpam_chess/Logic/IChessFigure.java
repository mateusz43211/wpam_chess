package com.example.archess.wpam_chess.Logic;

import java.util.ArrayList;

public interface IChessFigure {
    ArrayList <String> getPossibleMoves();
    void setPossibleMoves(ArrayList<String> possibleMoves);
    String getPosition();
    void setPosition(String position);
    String getName();
    String getColor();
    String getType();
}
