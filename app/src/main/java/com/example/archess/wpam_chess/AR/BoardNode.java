package com.example.archess.wpam_chess.AR;

import android.util.Log;

import com.example.archess.wpam_chess.Logic.IChessFigure;
import com.example.archess.wpam_chess.Logic.IGameManager;
import com.example.archess.wpam_chess.Logic.ITile;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.collision.Box;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;

import java.util.ArrayList;

class BoardNode extends Node {
    private static final String TAG = "BoardNode";
    private final IGameManager gameManager;
    private IChessFigure lastOpponentFigure;
    private IChessFigure touchedFigure;
    private String touchedNodeName;
    private ArrayList<Vector3> whitesHeap = new ArrayList<>();
    private ArrayList<Vector3> blacksHeap = new ArrayList<>();
    private int whitesIterator = 0;
    private int blacksIterator = 0;

    BoardNode(IGameManager gameManager) {
        super();
        this.gameManager = gameManager;

        setOnTapListener(
                (hitTestResult, motionEvent) -> {
                    Log.d(TAG, "TAG Chessboard touched");
                });
    }

    void onChessFigureTouched(IChessFigure chessFigure, String name) {
        if (gameManager.isPlayerTurn()) {
            // touch figures only when is player turn
            String playerTurn = gameManager.getPlayerTurn();
            if (touchedFigure == null) {
                if (chessFigure.getColor().equals(playerTurn)) {
                    enableFigureTiles(chessFigure);
                    touchedFigure = chessFigure;
                    touchedNodeName = name;
                }
            } else {
                if (touchedFigure == chessFigure) {
                    disableFigureTiles(touchedFigure);
                    touchedFigure = null;
                    touchedNodeName = null;
                }
                else if (touchedFigure.getColor().equals(chessFigure.getColor())) {
                    // another figure with the same color was chosen
                    disableFigureTiles(touchedFigure);
                    enableFigureTiles(chessFigure);
                    touchedFigure = chessFigure;
                    touchedNodeName = name;
                }
                else if (!chessFigure.getColor().equals(playerTurn)) {
                    // another figure with different color was chosen
                    String pos = chessFigure.getPosition();
                    ITile tile = gameManager.getTile(pos);
                    TileNode tileNode = (TileNode) findByName(pos);
                    if (tile.isOccupied() && (tileNode != null && tileNode.isPossibleMove())) {
                        moveFigurePlayer(touchedFigure.getPosition(), chessFigure.getPosition());
                    }
                }
            }
        }
    }

    private void disableFigureTiles(IChessFigure chessFigure) {
        // disable active tile
        String figurePosition = chessFigure.getPosition();
        TileNode tile = (TileNode) findByName(figurePosition);
        if (tile != null) {
            tile.setTileState(TileNode.TileState.FREE);
        }

        ArrayList<String> possibleMoves = chessFigure.getPossibleMoves();
        TileNode tileNode;
        for (String pos : possibleMoves) {
            tileNode = (TileNode) findByName(pos);
            if (tileNode != null){
                tileNode.setTileState(TileNode.TileState.FREE);
            }
        }
    }

    private void enableFigureTiles(IChessFigure chessFigure) {
        String figurePosition = chessFigure.getPosition();
        TileNode tile = (TileNode) findByName(figurePosition);
        if (tile != null) {
            tile.setTileState(TileNode.TileState.OCCUPIED);
        }

        ArrayList<String> possibleMoves = chessFigure.getPossibleMoves();
        TileNode tileNode;
        Log.d(TAG, "Positions: " + possibleMoves.toString());
        for (String pos : possibleMoves) {
            tileNode = (TileNode) findByName(pos);
            if (tileNode != null){
                tileNode.setTileState(TileNode.TileState.POSSIBLE_MOVE);
            }
        }
    }

    void moveFigurePlayer(String from, String target) {
        TileNode targetTileNode = (TileNode) findByName(target);
        IChessFigure figure = gameManager.getFigureByPosition(target);
        if (figure != null) {
            // tile is occupied
            if (targetTileNode != null && !figure.getColor().equals(touchedNodeName) && targetTileNode.isPossibleMove()) {
                moveOutFigure(figure.getName());
            }
        }

        FigureNode figureNode = (FigureNode) findByName(touchedNodeName);
        figureNode.setTarget(targetTileNode.getLocalPosition(), target, true);
        onChessFigureTouched(touchedFigure, figureNode.getName());
    }

    void moveAiFigure(IChessFigure figure, String target) {
        if (figure != null) {
            TileNode targetTileNode = (TileNode) findByName(target);
            FigureNode figureNode = (FigureNode) findByName(figure.getName());
            figureNode.setTarget(targetTileNode.getLocalPosition(), target, false);
        }
    }


    void moveOutFigure(String figureName) {
        FigureNode figureNode = (FigureNode) findByName(figureName);

        IChessFigure figure = gameManager.getFigureByName(figureName);
        String color = figure.getColor();
        Vector3 lastTarget = null;

        if (color.equals("w")) {
            lastTarget = whitesHeap.get(whitesIterator);
            whitesIterator++;
        } else if (color.equals("b")) {
            Log.d(TAG, "Black heap");
            lastTarget = blacksHeap.get(blacksIterator);
            blacksIterator++;
        }

        figureNode.moveOutFromBoard(lastTarget);
    }

    void generateOutPositions(float tileSize,
                              ModelRenderable possibleMoveModel) {
        generateHeaps(tileSize);

        for (Vector3 vec : whitesHeap) {
            Node node = new Node();
            node.setParent(this);
            node.setLocalPosition(vec);
            node.setRenderable(possibleMoveModel.makeCopy());
        }

        for (Vector3 vec : blacksHeap) {
            Node node = new Node();
            node.setParent(this);
            node.setLocalPosition(vec);
            node.setRenderable(possibleMoveModel.makeCopy());
        }
    }

    private void generateHeaps(float tileSize) {
        final float X_OFFSET = 2f;

        Box boundingBox = (Box) getCollisionShape();
        Vector3 size = boundingBox.getExtents();

        float x, y, z;

        x = size.x + X_OFFSET;
        y = -size.x;
        y = 0;
        z = -size.z;
        for (int i = 0; i < 8; i++) {
            whitesHeap.add(new Vector3(x, y + i * tileSize, z));
        }
        x += tileSize;
        for (int i = 0; i < 8; i++) {
            whitesHeap.add(new Vector3(x, y + i * tileSize, z));
        }

        x = -size.x - X_OFFSET;
        y = 0;

        for (int i = 0; i < 8; i++) {
            blacksHeap.add(new Vector3(x, y + i * tileSize, z));
        }
        x -= tileSize;
        for (int i = 0; i < 8; i++) {
            blacksHeap.add(new Vector3(x, y + i * tileSize, z));
        }
    }

    void showHint(String from, String to) {
        TileNode fromTile = (TileNode) findByName(from);
        fromTile.setTileState(TileNode.TileState.HINT);
        TileNode toTile = (TileNode) findByName(to);
        toTile.setTileState(TileNode.TileState.HINT);
    }
}
