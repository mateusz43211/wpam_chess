package com.example.archess.wpam_chess.Logic;

import com.example.archess.wpam_chess.OnGameListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Chessboard {
    private final Map<String, ITile> tileMap;
    private Map<String, IChessFigure> figureMap;
    private OnGameListener onGameListener;

    private ArrayList<IChessFigure> whites = new ArrayList<>();
    private ArrayList<IChessFigure> blacks = new ArrayList<>();

    public Chessboard(boolean isFromFen) {
        tileMap = ChessFactory.creteChessboard();
        figureMap = new HashMap<>();
        if (!isFromFen) {
            generateFigures();
            updateAllTiles();
        }
    }

    private void generateFigures() {
        // pion-„P”, skoczek-„N”, goniec-„B”, wieża-„R”, hetman-„Q” oraz król-„K”

        // whites
        addFigure('R', 1, 1);
        addFigure('N', 1, 2);
        addFigure('B', 1, 3);
        addFigure('Q', 1, 4);
        addFigure('K', 1, 5);
        addFigure('B', 1, 6);
        addFigure('N', 1, 7);
        addFigure('R', 1, 8);

        addFigure('P', 2, 1);
        addFigure('P', 2, 2);
        addFigure('P', 2, 3);
        addFigure('P', 2, 4);
        addFigure('P', 2, 5);
        addFigure('P', 2, 6);
        addFigure('P', 2, 7);
        addFigure('P', 2, 8);

        // blacks
        addFigure('r', 8, 1);
        addFigure('n', 8, 2);
        addFigure('b', 8, 3);
        addFigure('q', 8, 4);
        addFigure('k', 8, 5);
        addFigure('b', 8, 6);
        addFigure('n', 8, 7);
        addFigure('r', 8, 8);

        addFigure('p', 7, 1);
        addFigure('p', 7, 2);
        addFigure('p', 7, 3);
        addFigure('p', 7, 4);
        addFigure('p', 7, 5);
        addFigure('p', 7, 6);
        addFigure('p', 7, 7);
        addFigure('p', 7, 8);
    }

    public String moveFigure(String from, String to) {
        String moveFigureOut = null;
        ITile fromTile = tileMap.get(from);
        if (fromTile.isOccupied()) {
            IChessFigure currentFigure = fromTile.getFigure();
            ITile toTile = tileMap.get(to);
            if (toTile.isOccupied()) {
                // zbij pionek
                moveFigureOut = toTile.getFigure().getName();
            }

            toTile.setFigure(currentFigure);
            currentFigure.setPosition(to);
            fromTile.setFigure(null);
        }

        return moveFigureOut;
    }

    public void setListOfPossibleMoves(String position, ArrayList<String> possibleMoves) {
        ITile currentTile = tileMap.get(position);
        if (currentTile.isOccupied()) {
            IChessFigure figure = currentTile.getFigure();
            figure.setPossibleMoves(possibleMoves);
        }
    }

    public IChessFigure getFigureByPosition(String position) {
        return tileMap.get(position).getFigure();
    }

    public IChessFigure getFigureByName(String name) {
        return figureMap.get(name);
    }

    public ArrayList<IChessFigure> getAllFigures() {
        return new ArrayList<>(figureMap.values());
    }


    public ArrayList<IChessFigure> getWhites() {
        return whites;
    }

    public ArrayList<IChessFigure> getBlacks() {
        return blacks;
    }

    public void setOnGameListener(OnGameListener onGameListener) {
        this.onGameListener = onGameListener;
    }

    public void loadGameFromFEN(String[] rows) {
        for (int r = rows.length; r > 0; --r) {
            String current_row = rows[r-1];
            int column_index = 1;
            for (int c = 0; c < current_row.length(); c++) {
                char current_char = current_row.charAt(c);
                if (Character.isDigit(current_char)) {
                    // this is a gap
                    column_index += Character.getNumericValue(current_char);
                } else {
                    // this is a figure
                    addFigure(current_char, getRowForDigit(r), column_index);
                    column_index++;
                }
            }
        }
        updateAllTiles();
    }

    private int getRowForDigit(int value) {
        switch (value){
            case 1:
                return 8;
            case 2:
                return 7;
            case 3:
                return 6;
            case 4:
                return 5;
            case 5:
                return 4;
            case 6:
                return 3;
            case 7:
                return 2;
            case 8:
                return 1;
        }
        return 1;
    }

    private void addFigure(char figure_symbol, int row_index, int column_index) {
        IChessFigure figure = ChessFactory.createFigure(figure_symbol, row_index, column_index);
        figureMap.put(figure.getName(), figure);

        String color = figure.getColor();
        if (color.equals("w")) {
            whites.add(figure);
        } else if (color.equals("b")) {
            blacks.add(figure);
        }
    }

    private void updateAllTiles() {
        for (IChessFigure figure : figureMap.values()) {
            String pos = figure.getPosition();
            tileMap.get(pos).setFigure(figure);
        }
    }

    public ITile getTile(String position) {
        return tileMap.get(position);
    }

    public ArrayList<IChessFigure> getPlayerFigures(String playerColor) {
        return playerColor.equals("w") ? whites : blacks;
    }
}
