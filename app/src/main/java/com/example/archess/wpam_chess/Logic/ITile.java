package com.example.archess.wpam_chess.Logic;

public interface ITile {
    IChessFigure getFigure();
    void setFigure(IChessFigure figure);
    boolean isOccupied();
    String getPosition();
}
