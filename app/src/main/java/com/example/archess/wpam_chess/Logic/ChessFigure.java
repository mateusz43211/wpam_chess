package com.example.archess.wpam_chess.Logic;

import java.util.ArrayList;

public class ChessFigure implements IChessFigure {

    private final String type;
    private final String color;
    private final String name;
    private String position;
    private ArrayList<String> possibleMoves;

    public ChessFigure(String type, String color, String position, String name) {
        this.position = position;
        this.type = type;
        this.color = color;
        this.name = name;
    }

    @Override
    public ArrayList<String> getPossibleMoves() {
        return possibleMoves;
    }

    @Override
    public void setPossibleMoves(ArrayList<String> possibleMoves) {
        this.possibleMoves = possibleMoves;
    }

    @Override
    public String getPosition() {
        return position;
    }

    @Override
    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getType() {
        return type;
    }
}
