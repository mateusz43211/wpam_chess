package com.example.archess.wpam_chess.AR;

public interface OnLoadingFinishedListener {
    void onLoadingFinished();
}
