package com.example.archess.wpam_chess.TwoPlayers;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.CHECK_GAME_OVER;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.GET_FEN;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.GET_TURN;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.LIST_POSSIBLE_MOVES;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.MOVE_FIGURE_PLAYER;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.NEW_GAME;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.RESET_BOARD;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.CHECK_GAME_OVER_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.ERROR;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.GET_FEN_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.GET_TURN_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.LIST_POSSIBLE_MOVES_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.MOVE_FIGURE_PLAYER_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.NEW_GAME_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.RUNNING;

public class TwoPlayersQueryService extends IntentService {

    public TwoPlayersQueryService() {
        super("TwoPlayersQueryService");
    }

    public TwoPlayersQueryService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String command = intent.getStringExtra("command");
        Bundle b = new Bundle();
        switch (command) {
            case NEW_GAME:
                newGame(receiver, b);
                break;
            case LIST_POSSIBLE_MOVES:
                listPossibleMoves(intent, receiver, b);
                break;
            case MOVE_FIGURE_PLAYER:
                moveFigurePlayer(intent, receiver, b);
                break;
            case CHECK_GAME_OVER:
                checkGameOver(intent, receiver, b);
                break;
            case GET_TURN:
                getTurn(intent, receiver, b);
                break;
            case RESET_BOARD:
                // resetBoard(intent, receiver, b);
                break;
            case GET_FEN:
                getFen(intent, receiver, b);
                break;
        }
    }

    private void newGame(ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        try {
            JsonObject jsonObject = TwoPlayersChessApiClient.getStartGame();
            if (jsonObject != null) {
                String game_id = "game_id";
                String status = "status";

                JsonElement element = jsonObject.get(game_id);
                b.putString(game_id, element.getAsString());

                element = jsonObject.get(status);
                b.putString(status, element.getAsString());

                receiver.send(NEW_GAME_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void listPossibleMoves(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String position = intent.getStringExtra("position");
        String game_id = intent.getStringExtra("game_id");

        try {
            JsonObject jsonObject = TwoPlayersChessApiClient.getListPossibleMoves(position, game_id);
            if (jsonObject != null) {
                String moves = "moves";
                JsonElement element = jsonObject.get(moves);

                Type arrayListType = new TypeToken<ArrayList<String>>() {
                }.getType();
                ArrayList<String> possibleMoves = new Gson().fromJson(element, arrayListType);

                b.putStringArrayList(moves, possibleMoves);
                b.putString("position", position);
                receiver.send(LIST_POSSIBLE_MOVES_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void moveFigurePlayer(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String from = intent.getStringExtra("from");
        String to = intent.getStringExtra("to");
        String game_id = intent.getStringExtra("game_id");

        try {
            JsonObject jsonObject = TwoPlayersChessApiClient.getMoveFigurePlayer(from, to, game_id);
            if (jsonObject != null) {
                String status = "status";
                JsonElement element = jsonObject.get(status);
                b.putString(status, element.getAsString());
                receiver.send(MOVE_FIGURE_PLAYER_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void checkGameOver(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String game_id = intent.getStringExtra("game_id");
        try {
            JsonObject jsonObject = TwoPlayersChessApiClient.getCheckGameOver(game_id);
            if (jsonObject != null) {
                String status = "status";

                JsonElement elementStatus = jsonObject.get(status);
                b.putString(status, elementStatus.getAsString());

                receiver.send(CHECK_GAME_OVER_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void getTurn(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String game_id = intent.getStringExtra("game_id");
        try {
            JsonObject jsonObject = TwoPlayersChessApiClient.getTurn(game_id);
            if (jsonObject != null) {
                String turn = "turn";

                JsonElement elementTurn = jsonObject.get(turn);
                b.putString(turn, elementTurn.getAsString());

                receiver.send(GET_TURN_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void getFen(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String game_id = intent.getStringExtra("game_id");
        try {
            JsonObject jsonObject = TwoPlayersChessApiClient.getFen(game_id);
            if (jsonObject != null) {
                String fen_string = "fen_string";

                JsonElement elementTurn = jsonObject.get(fen_string);
                b.putString(fen_string, elementTurn.getAsString());
                
                receiver.send(GET_FEN_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }
}
