package com.example.archess.wpam_chess.TwoPlayers;

import com.example.archess.wpam_chess.Utils;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.CHECK_GAME_OVER;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.GET_FEN;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.GET_TURN;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.LIST_POSSIBLE_MOVES;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.MOVE_FIGURE_PLAYER;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.NEW_GAME;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.commandURLMap;

class TwoPlayersChessApiClient {
    private static final String GET_METHOD_NAME = "GET";
    private static final String POST_METHOD_NAME = "POST";

    static JsonObject getStartGame() {
        HttpURLConnection connection = null;
        try {
            connection = getStartGameConnection();
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    private static HttpURLConnection getStartGameConnection() throws IOException {
        String path = commandURLMap.get(NEW_GAME);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(GET_METHOD_NAME);
        return connection;
    }

    static JsonObject getListPossibleMoves(String position, String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getListOfPossibleMovesConnection(position, game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    private static HttpURLConnection getListOfPossibleMovesConnection(String position,
                                                                      String game_id) throws IOException {
        String path = commandURLMap.get(LIST_POSSIBLE_MOVES);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("position=").append(position);
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;

    }

    static JsonObject getMoveFigurePlayer(String from, String to, String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getMoveFigurePlayerConnection(from, to, game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    private static HttpURLConnection getMoveFigurePlayerConnection(String from, String to,
                                                                   String game_id) throws IOException {
        String path = commandURLMap.get(MOVE_FIGURE_PLAYER);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("from=").append(from);
        builder.append("&to=").append(to);
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());

        return connection;

    }

    static JsonObject getCheckGameOver(String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getCheckGameOverConnection(game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    private static HttpURLConnection getCheckGameOverConnection(
            String game_id) throws IOException {
        String path = commandURLMap.get(CHECK_GAME_OVER);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;
    }

    static JsonObject getTurn(String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getTurnConnection(game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    private static HttpURLConnection getTurnConnection(
            String game_id) throws IOException {
        String path = commandURLMap.get(GET_TURN);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;
    }

    static JsonObject getFen(String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getFenConnection(game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    private static HttpURLConnection getFenConnection(
            String game_id) throws IOException {
        String path = commandURLMap.get(GET_FEN);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;
    }


}
