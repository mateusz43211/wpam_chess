package com.example.archess.wpam_chess.TwoPlayers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.archess.wpam_chess.Logic.Chessboard;
import com.example.archess.wpam_chess.Logic.GameManager;
import com.example.archess.wpam_chess.Logic.IChessFigure;
import com.example.archess.wpam_chess.Logic.ITile;
import com.example.archess.wpam_chess.Logic.MoveValidator;
import com.example.archess.wpam_chess.OnGameListener;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.GET_TURN;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.LIST_POSSIBLE_MOVES;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.MOVE_FIGURE_PLAYER;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.CHECK_GAME_OVER_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.GET_FEN_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.GET_TURN_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.LIST_POSSIBLE_MOVES_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.MOVE_FIGURE_PLAYER_FINISHED;
import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.ServiceStatus.RUNNING;

public class GameManagerTwoPlayers extends GameManager {
    private static final String TAG = "GameManagerTwoPlayers";
    private Chessboard chessboard;
    private String figurePosition, figureTarget;
    private String playerTurn;
    private String playerColor;
    private String currentFEN;
    private Timer timer;
    private boolean isGameStarted = false;
    private boolean loadCurrentFEN = false;
    private boolean isOpponentMove = false;


    public GameManagerTwoPlayers(Context context, String gameId, String playerColor) {
        super(context);
        this.GAME_ID = gameId;
        this.playerColor = playerColor;
        chessboard = new Chessboard(true);
    }

    @Override
    public void start() {
        getFEN();
        getPlayerTurn();
    }

    private void getFEN() {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, TwoPlayersQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", ConstantsTwoPlayer.GET_FEN);
        intent.putExtra("game_id", GAME_ID);
        Log.d(TAG, "Game id: " + GAME_ID);
        context.startService(intent);
    }

    @Override
    public void checkGameOver() {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, TwoPlayersQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", ConstantsTwoPlayer.CHECK_GAME_OVER);
        intent.putExtra("game_id", GAME_ID);
        Log.d(TAG, "Game id: " + GAME_ID);
        context.startService(intent);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case RUNNING:
                break;

            case GET_FEN_FINISHED:
                String fen_string = resultData.getString("fen_string");
                Log.d(TAG, "Get FEN: " + fen_string);
                if (!isGameStarted) {

                    loadGameFromFen(fen_string);
                    loadPlayerTurn();
                    reloadPossibleMoves();


                    isGameStarted = true;
                } else if (loadCurrentFEN) {
                    currentFEN = fen_string;
                    loadCurrentFEN = false;
                } else if (playerTurn.equals(playerColor) && isOpponentMove) {
                    // getMove
                    ArrayList<String> move = MoveValidator.getMoveFromStatus(currentFEN, fen_string);
                    if (move.size() == 2) {
                        onGameListener.OnMoveAI(move.get(0), move.get(1));
                    } else if (move.size() == 4) {
                        // castle
                        onGameListener.OnMoveAI(move.get(0), move.get(1));
                        onGameListener.OnMoveAI(move.get(2), move.get(3));
                    }

                    String moveFigureOutName = chessboard.moveFigure(move.get(0), move.get(1));
                    if (moveFigureOutName != null) {
                        onGameListener.onMoveOutFigure(moveFigureOutName);
                    }
                    reloadPossibleMoves();
                    loadPlayerTurn();

                }

                break;
            case LIST_POSSIBLE_MOVES_FINISHED: {
                ArrayList<String> possibleMoves = resultData.getStringArrayList("moves");
                Log.d(TAG, "List possible moves: " + possibleMoves.toString());

                if (possibleMoves.contains("7+")) {
                    int index = possibleMoves.indexOf("7+");
                    possibleMoves.set(index, "f7");
                }
                String position = resultData.getString("position");

                chessboard.setListOfPossibleMoves(position, possibleMoves);

                break;
            }

            case MOVE_FIGURE_PLAYER_FINISHED: {
                String status = resultData.getString("status");

                Log.d(TAG, "MoveFigure player   from: " + figurePosition + " to: " + figureTarget);
                chessboard.moveFigure(figurePosition, figureTarget);
                onGameListener.onMovePlayer(false);

                checkGameOver();

                loadCurrentFEN = true;
                getFEN();

                loadPlayerTurn();
                awaitForOpponentMove();
                isOpponentMove = false;

                break;
            }

            case CHECK_GAME_OVER_FINISHED: {
                String status = resultData.getString("status");
                Log.d(TAG, "Game status: " + status);
                if (!status.equals("game continues")) {
                    onGameListener.onGameOver(getPlayerTurn());
                }

                break;
            }

            case GET_TURN_FINISHED: {
                String currentTurn = resultData.getString("turn");
                Log.d(TAG, "Get turn: " + currentTurn);
                if (playerTurn != null) {
                    if (currentTurn.equals(playerColor) && !isOpponentMove) {
                        timer.cancel();
                        timer.purge();
                        // load state and get move of opponent
                        playerTurn = currentTurn;
                        isOpponentMove = true;
                        getFEN();
                        checkGameOver();
                        onGameListener.onMovePlayer(true);
                    }
                } else {
                    playerTurn = currentTurn;
                    reloadPossibleMoves();
                }


                break;
            }
        }
    }

    private void loadGameFromFen(String fen_string) {
        int space_index = fen_string.indexOf(" ");

        String positions = fen_string.substring(0, space_index);
        String rows[] = positions.split("/");

        chessboard.loadGameFromFEN(rows);
    }

    private void awaitForOpponentMove() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                loadPlayerTurn();
                Log.d(TAG, "Timer task execution");
            }
        }, 0, 3000);

    }

    private void listPossibleMoves(String position) {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, TwoPlayersQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", LIST_POSSIBLE_MOVES);
        intent.putExtra("position", position);
        intent.putExtra("game_id", GAME_ID);
        context.startService(intent);
    }

    private void loadPlayerTurn() {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, TwoPlayersQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", GET_TURN);
        intent.putExtra("game_id", GAME_ID);
        context.startService(intent);
    }

    private void reloadPossibleMoves() {
        for (IChessFigure figure : chessboard.getPlayerFigures(playerColor)) {
            listPossibleMoves(figure.getPosition());
        }
    }

    public void setOnGameListener(OnGameListener onGameListener) {
        this.onGameListener = onGameListener;
        chessboard.setOnGameListener(onGameListener);
    }

    @Override
    public void movePlayer(String from, String to) {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, TwoPlayersQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", MOVE_FIGURE_PLAYER);
        intent.putExtra("from", from);
        intent.putExtra("to", to);
        intent.putExtra("game_id", GAME_ID);
        context.startService(intent);

        figurePosition = from;
        figureTarget = to;
    }

    @Override
    public IChessFigure getFigureByPosition(String position) {
        return chessboard.getFigureByPosition(position);
    }

    @Override
    public IChessFigure getFigureByName(String name) {
        return chessboard.getFigureByName(name);
    }

    @Override
    public ArrayList<IChessFigure> getAllFigures() {
        return chessboard.getAllFigures();
    }

    @Override
    public String getPlayerColor() {
        return playerColor;
    }

    @Override
    public String getPlayerTurn() {
        return playerTurn;
    }

    @Override
    public boolean isPlayerTurn() {
        return playerColor.equals(playerTurn);
    }

    @Override
    public ITile getTile(String position) {
        return chessboard.getTile(position);
    }

    @Override
    public void getHint() {
        // no hint in two players mode
    }

    @Override
    public boolean isTwoPlayerGame() {
        return true;
    }
}
