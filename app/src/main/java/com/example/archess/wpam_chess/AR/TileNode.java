package com.example.archess.wpam_chess.AR;

import android.util.Log;

import com.example.archess.wpam_chess.Logic.ITile;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.rendering.ModelRenderable;

class TileNode extends Node {

    public enum TileState {
        POSSIBLE_MOVE,
        OCCUPIED,
        FREE,
        HINT
    }

    private static final String TAG = "TileNode";
    private ModelRenderable currentTile;
    private ModelRenderable possibleTile;
    private ModelRenderable occupiedByOpponentTile;
    private ModelRenderable hintTile;
    private TileState tileState;
    private final ITile tile;

    TileNode(ITile tile, ModelRenderable possibleMove, ModelRenderable occupiedByPlayer,
             ModelRenderable occupiedByOpponent, ModelRenderable hint) {
        super();

        this.tile = tile;
        currentTile = occupiedByPlayer;
        possibleTile = possibleMove;
        occupiedByOpponentTile = occupiedByOpponent;
        this.hintTile = hint;

        setOnTapListener((hitTestResult, motionEvent) -> {
            Log.d(TAG, "TAG TileNode touched");

            if (tileState == TileState.POSSIBLE_MOVE) {
                BoardNode boardNode = (BoardNode) getParent();
                if (boardNode != null) {
                    boardNode.moveFigurePlayer(tile.getPosition(), getName());
                }
            }
        });
    }

    void setTileState(TileState tileState) {
        this.tileState = tileState;
        switch (tileState) {
            case POSSIBLE_MOVE:
                if (tile.isOccupied()) {
                    setRenderable(occupiedByOpponentTile);
                } else {
                    setRenderable(possibleTile);
                }
                break;
            case OCCUPIED:
                setRenderable(currentTile);
                break;
            case FREE:
                setRenderable(null);
                break;
            case HINT:
                setRenderable(hintTile);
                break;
        }
    }

    boolean isPossibleMove() {
        return tileState == TileState.POSSIBLE_MOVE;
    }
}
