package com.example.archess.wpam_chess.TwoPlayers;

import android.app.ActivityOptions;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.archess.wpam_chess.AR.ARCoreActivity;
import com.example.archess.wpam_chess.MyResultReceiver;
import com.example.archess.wpam_chess.R;
import com.example.archess.wpam_chess.Utils;

import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.CHECK_GAME_OVER;


public class JoinGameActivity extends AppCompatActivity implements MyResultReceiver.Receiver {

    private MyResultReceiver myResultReceiver;
    private String GAME_ID;
    private String playerColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_game);

        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);

        EditText gameIdEditText = findViewById(R.id.gameIdEditText);
        Button joinGameButton = findViewById(R.id.joinGameButton);
        joinGameButton.setOnClickListener(v -> {
            String game_id = gameIdEditText.getText().toString();
            checkGameStatus(game_id);
        });

        ImageButton pasteGameIdButton = findViewById(R.id.pasteGameIdButton);
        pasteGameIdButton.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData.Item clipItem= clipboard.getPrimaryClip().getItemAt(0);

            String game_id = clipItem.getText().toString();
            gameIdEditText.setText(game_id);
        });

        playerColor = getIntent().getStringExtra("color");
        if (playerColor == null) {
            playerColor = "b";
        }
    }

    private void checkGameStatus(String game_id) {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, getApplicationContext(), TwoPlayersQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", CHECK_GAME_OVER);
        intent.putExtra("game_id", game_id);
        startService(intent);

        this.GAME_ID = game_id;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case ConstantsTwoPlayer.ServiceStatus.RUNNING:
                break;

            case ConstantsTwoPlayer.ServiceStatus.CHECK_GAME_OVER_FINISHED:
                String status = resultData.getString("status");

                if (status != null && status.equals("game continues")){
                    // start game
                    Intent intent = new Intent(JoinGameActivity.this, ARCoreActivity.class);
                    intent.putExtra("game_id", GAME_ID);
                    intent.putExtra("number_of_players", "two");
                    intent.putExtra("color", playerColor);
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

                }
                else {
                    // error
                    Utils.showToastWithMessage(this, "The game has expired");
                }


                break;
            case ConstantsTwoPlayer.ServiceStatus.ERROR:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        myResultReceiver.setReceiver(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        myResultReceiver.setReceiver(null);
    }
}
