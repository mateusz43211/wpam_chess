package com.example.archess.wpam_chess.OnePlayer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.archess.wpam_chess.Logic.Chessboard;
import com.example.archess.wpam_chess.Logic.GameManager;
import com.example.archess.wpam_chess.Logic.IChessFigure;
import com.example.archess.wpam_chess.Logic.ITile;
import com.example.archess.wpam_chess.OnGameListener;

import java.util.ArrayList;

import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.CHECK_GAME_OVER;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.GET_TURN;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.LIST_POSSIBLE_MOVES;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.MOVE_FIGURE_AI;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.MOVE_FIGURE_PLAYER;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.NEW_GAME;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.PLAYER_HELP;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.CHECK_GAME_OVER_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.ERROR;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.GET_TURN_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.LIST_POSSIBLE_MOVES_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.MOVE_FIGURE_AI_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.MOVE_FIGURE_PLAYER_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.NEW_GAME_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.PLAYER_HELP_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.RUNNING;

public class GameManagerOnePlayer extends GameManager {
    private static final String TAG = "GameManagerOnePlayer";
    private Chessboard chessboard;
    private final String playerColor = "w";
    private String playerTurn;
    private String figureTarget, figurePosition;

    public GameManagerOnePlayer(Context context) {
        super(context);
        chessboard = new Chessboard(false);
    }

    @Override
    public void start() {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, OnePlayerQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", NEW_GAME);
        context.startService(intent);
    }

    @Override
    public void checkGameOver() {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, OnePlayerQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", CHECK_GAME_OVER);
        intent.putExtra("game_id", GAME_ID);
        context.startService(intent);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case RUNNING:
                break;
            case NEW_GAME_FINISHED: {
                GAME_ID = resultData.getString("game_id");
                Log.d(TAG, "New game id: " + GAME_ID);
                reloadPossibleMoves();
                loadPlayerTurn();
                break;
            }

            case LIST_POSSIBLE_MOVES_FINISHED: {
                ArrayList<String> possibleMoves = resultData.getStringArrayList("moves");

                if (possibleMoves != null) {
                    Log.d(TAG, "List possible moves: " + possibleMoves.toString());

                    if (possibleMoves.contains("7+")) {
                        int index = possibleMoves.indexOf("7+");
                        possibleMoves.set(index, "f7");
                    }
                    if (possibleMoves.contains("6+")) {
                        int index = possibleMoves.indexOf("6+");
                        possibleMoves.set(index, "c7");
                    }

                    String position = resultData.getString("position");

                    chessboard.setListOfPossibleMoves(position, possibleMoves);
                }
                else {
                    Log.d(TAG, "List possible move unsuccessful");
                }

                break;
            }

            case MOVE_FIGURE_AI_FINISHED: {
                String from = resultData.getString("from");
                String to = resultData.getString("to");
                Log.d(TAG, "MoveFigure AI  from: " + from + " to: " + to);
                // update graphic and next logic
                onGameListener.OnMoveAI(from, to);

                String moveFigureOutName = chessboard.moveFigure(from, to);
                if(moveFigureOutName != null){
                    onGameListener.onMoveOutFigure(moveFigureOutName);
                }

                checkGameOver();
                reloadPossibleMoves();
                loadPlayerTurn();
                break;
            }


            case MOVE_FIGURE_PLAYER_FINISHED: {
                String status = resultData.getString("status");

                Log.d(TAG, "MoveFigure player   from: " + figurePosition + " to: " + figureTarget);
                chessboard.moveFigure(figurePosition, figureTarget);

                playerTurn = "b";
                onGameListener.onMovePlayer(false);
                checkGameOver();

                loadPlayerTurn();



                moveAI();

                break;
            }

            case CHECK_GAME_OVER_FINISHED: {
                String status = resultData.getString("status");
                Log.d(TAG, "Game status: " + status);
                if (!status.equals("game continues")) {
                    onGameListener.onGameOver(getPlayerTurn());
                }
                break;
            }

            case GET_TURN_FINISHED: {
                playerTurn = resultData.getString("turn");
                break;
            }

            case PLAYER_HELP_FINISHED: {
                String status = resultData.getString("status");
                String from = resultData.getString("from");
                String to = resultData.getString("to");

                if (status.equals("Player should move:")) {
                    onGameListener.showHint(from, to);
                }
                break;
            }

            case ERROR:
                break;
        }
    }

    private void moveAI() {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, OnePlayerQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", MOVE_FIGURE_AI);
        intent.putExtra("game_id", GAME_ID);
        context.startService(intent);
    }

    private void listPossibleMoves(String position) {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, OnePlayerQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", LIST_POSSIBLE_MOVES);
        intent.putExtra("position", position);
        intent.putExtra("game_id", GAME_ID);
        context.startService(intent);
    }

    private void loadPlayerTurn() {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, OnePlayerQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", GET_TURN);
        intent.putExtra("game_id", GAME_ID);
        context.startService(intent);
    }

    private void reloadPossibleMoves() {
        for (IChessFigure figure : chessboard.getPlayerFigures(playerColor)) {
            listPossibleMoves(figure.getPosition());
        }
    }

    public void setOnGameListener(OnGameListener onGameListener) {
        this.onGameListener = onGameListener;
        chessboard.setOnGameListener(onGameListener);
    }

    @Override
    public void movePlayer(String from, String to) {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, OnePlayerQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", MOVE_FIGURE_PLAYER);
        intent.putExtra("from", from);
        intent.putExtra("to", to);
        intent.putExtra("game_id", GAME_ID);
        context.startService(intent);

        figurePosition = from;
        figureTarget = to;
    }

    @Override
    public IChessFigure getFigureByPosition(String position) {
        return chessboard.getFigureByPosition(position);
    }

    @Override
    public IChessFigure getFigureByName(String name) {
        return chessboard.getFigureByName(name);
    }

    @Override
    public ArrayList<IChessFigure> getAllFigures() {
        return chessboard.getAllFigures();
    }

    @Override
    public String getPlayerColor() {
        return playerColor;
    }

    @Override
    public String getPlayerTurn() {
        return playerTurn;
    }

    @Override
    public boolean isPlayerTurn() {
        return playerColor.equals(playerTurn);
    }

    @Override
    public ITile getTile(String position) {
        return chessboard.getTile(position);
    }

    @Override
    public void getHint() {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, context, OnePlayerQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", PLAYER_HELP);
        intent.putExtra("game_id", GAME_ID);
        context.startService(intent);
    }

    @Override
    public boolean isTwoPlayerGame() {
        return false;
    }
}
