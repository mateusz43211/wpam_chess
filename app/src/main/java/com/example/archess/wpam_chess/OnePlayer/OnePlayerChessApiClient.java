package com.example.archess.wpam_chess.OnePlayer;

import com.example.archess.wpam_chess.Utils;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.CHECK_GAME_OVER;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.GET_TURN;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.LIST_POSSIBLE_MOVES;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.MOVE_FIGURE_AI;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.MOVE_FIGURE_PLAYER;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.NEW_GAME;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.PLAYER_HELP;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.RESET_BOARD;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.UNDO_MOVE;

public class OnePlayerChessApiClient {
    private static final String GET_METHOD_NAME = "GET";
    private static final String POST_METHOD_NAME = "POST";


    static JsonObject getStartGame() {
        HttpURLConnection connection = null;
        try {
            connection = getStartGameConnection();
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    static JsonObject getListPossibleMoves(String position, String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getListOfPossibleMovesConnection(position, game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    static JsonObject getMoveFigurePlayer(String from, String to,
                                          String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getMoveFigurePlayerConnection(from, to, game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    static JsonObject getMoveFigureAI(String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getMoveFigureAIConnection(game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    static JsonObject getCheckGameOver(String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getCheckGameOverConnection(game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    static JsonObject getPlayerHelp(String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getPlayerHelpConnection(game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    static JsonObject getTurn(String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getTurnConnection(game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    private static JsonObject undoMove(String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getUndoMoveConnection(game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }

    private static JsonObject resetBoard(String game_id) {
        HttpURLConnection connection = null;
        try {
            connection = getResetBoardConnection(game_id);
            return Utils.getResponseJsonObject(connection);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
        return null;
    }


    private static HttpURLConnection getStartGameConnection() throws IOException {
        String path = ConstantsOnePlayer.commandURLMap.get(NEW_GAME);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(GET_METHOD_NAME);
        return connection;
    }

    private static HttpURLConnection getListOfPossibleMovesConnection(String position,
                                                                      String game_id) throws IOException {
        String path = ConstantsOnePlayer.commandURLMap.get(LIST_POSSIBLE_MOVES);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("position=").append(position);
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;
    }

    private static HttpURLConnection getMoveFigurePlayerConnection(String from, String to,
                                                                   String game_id) throws IOException {
        String path = ConstantsOnePlayer.commandURLMap.get(MOVE_FIGURE_PLAYER);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("from=").append(from);
        builder.append("&to=").append(to);
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());

        return connection;
    }

    private static HttpURLConnection getMoveFigureAIConnection(String game_id) throws IOException {
        String path = ConstantsOnePlayer.commandURLMap.get(MOVE_FIGURE_AI);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());

        return connection;
    }

    private static HttpURLConnection getCheckGameOverConnection(String game_id) throws IOException {
        String path = ConstantsOnePlayer.commandURLMap.get(CHECK_GAME_OVER);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;
    }

    private static HttpURLConnection getPlayerHelpConnection(String game_id) throws IOException {
        String path = ConstantsOnePlayer.commandURLMap.get(PLAYER_HELP);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;
    }

    private static HttpURLConnection getTurnConnection(String game_id) throws IOException {
        String path = ConstantsOnePlayer.commandURLMap.get(GET_TURN);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;
    }

    private static HttpURLConnection getUndoMoveConnection(String game_id) throws IOException {
        String path = ConstantsOnePlayer.commandURLMap.get(UNDO_MOVE);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;
    }

    private static HttpURLConnection getResetBoardConnection(String game_id) throws IOException {
        String path = ConstantsOnePlayer.commandURLMap.get(RESET_BOARD);
        HttpURLConnection connection = (HttpURLConnection) new URL(path).openConnection();
        connection.setRequestMethod(POST_METHOD_NAME);

        StringBuilder builder = new StringBuilder();
        builder.append("&game_id=").append(game_id);
        String data = builder.toString();

        connection.setDoOutput(true);
        connection.getOutputStream().write(data.getBytes());
        return connection;
    }


}
