package com.example.archess.wpam_chess.AR;

import android.util.Log;

import com.example.archess.wpam_chess.Logic.IChessFigure;
import com.example.archess.wpam_chess.Logic.IGameManager;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.collision.Box;
import com.google.ar.sceneform.math.Vector3;

import java.util.ArrayList;

import static com.example.archess.wpam_chess.AR.ConstantsAR.PAWN_SCALE_FACTOR;

public class FigureNode extends Node {
    private static final String TAG = "FigureNode";
    private final IChessFigure chessFigure;
    private IGameManager gameManager;
    private boolean moveFigurePlayer = false;
    private static final float UP_DISTANCE = 0.3f;

    private String targetName;
    private Vector3 targetPosition;
    private final float SPEED = 0.5f;
    private ArrayList<Vector3> moveOutSteps;


    FigureNode(IChessFigure chessFigure, IGameManager gameManager) {
        super();

        this.chessFigure = chessFigure;
        this.gameManager = gameManager;

        setOnTapListener((hitTestResult, motionEvent) -> {
            Log.d(TAG, "TAG FigureNode Touched");
            BoardNode boardNode = (BoardNode) getParent();
            if (boardNode != null) {
                boardNode.onChessFigureTouched(chessFigure, getName());
            }
        });
    }

    @Override
    public void onUpdate(FrameTime frameTime) {
        super.onUpdate(frameTime);

        if (targetPosition != null) {
            Vector3 localPosition = getLocalPosition();
            Box box = (Box) getCollisionShape();
            Vector3 temp = new Vector3(0f, -box.getExtents().y, box.getExtents().z).scaled(PAWN_SCALE_FACTOR);
            Vector3 targetForFigure = Vector3.add(targetPosition, temp);
            Vector3 direction = Vector3.subtract(targetForFigure, localPosition);

            if (direction.length() > SPEED) {
                Vector3 offset = direction.normalized().scaled(SPEED);
                localPosition = Vector3.add(localPosition, offset);
                setLocalPosition(localPosition);
            } else {
                setLocalPosition(targetForFigure);

                if (moveFigurePlayer) {
                    gameManager.movePlayer(chessFigure.getPosition(), targetName);
                }

                targetPosition = null;
                targetName = null;
                moveFigurePlayer = false;
            }
        } else if (moveOutSteps != null) {
            if (moveOutSteps.size() != 0) {
                targetPosition = moveOutSteps.get(moveOutSteps.size() - 1);
                moveOutSteps.remove(moveOutSteps.size() - 1);
            } else {
                moveOutSteps = null;
                setOnTapListener(null);
            }
        }
    }

    void setTarget(Vector3 targetPosition, String targetName, boolean playerMove) {
        this.targetPosition = targetPosition;
        this.targetName = targetName;
        this.moveFigurePlayer = playerMove;
    }

    void moveOutFromBoard(Vector3 lastTarget) {
        ArrayList<Vector3> moveOutPositions = new ArrayList<>();

        Box box = (Box) getCollisionShape();
        Vector3 offset = new Vector3(0f, -box.getExtents().y, box.getExtents().z).scaled(PAWN_SCALE_FACTOR);

        moveOutPositions.add(lastTarget);

        // go above
        Vector3 localPosition = getLocalPosition();
        Vector3 firstTarget = new Vector3(localPosition.x, localPosition.y, localPosition.z + UP_DISTANCE);
        firstTarget = Vector3.add(firstTarget, offset);
        Vector3 secondTarget = new Vector3(lastTarget.x, lastTarget.y, firstTarget.z);

        moveOutPositions.add(secondTarget);
        moveOutPositions.add(firstTarget);

        moveOutSteps = moveOutPositions;
    }

}
