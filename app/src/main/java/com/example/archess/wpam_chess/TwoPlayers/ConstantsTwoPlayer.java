package com.example.archess.wpam_chess.TwoPlayers;

import java.util.HashMap;
import java.util.Map;

class ConstantsTwoPlayer {
    class ServiceStatus {
        static final int RUNNING = 1;
        static final int NEW_GAME_FINISHED = 2;
        static final int LIST_POSSIBLE_MOVES_FINISHED = 3;
        static final int MOVE_FIGURE_PLAYER_FINISHED = 4;
        static final int CHECK_GAME_OVER_FINISHED = 5;
        static final int GET_TURN_FINISHED = 6;
        static final int GET_FEN_FINISHED = 7;
        static final int RESET_BOARD_FINISHED = 8;
        static final int ERROR = 9;
    }

    static final String NEW_GAME = "NEW_GAME";
    static final String LIST_POSSIBLE_MOVES = "LIST_POSSIBLE_MOVES";
    static final String MOVE_FIGURE_PLAYER = "MOVE_FIGURE_PLAYER";
    static final String CHECK_GAME_OVER = "CHECK_GAME_OVER";
    static final String GET_TURN = "GET_TURN";
    static final String RESET_BOARD = "RESET_BOARD";
    static final String GET_FEN = "GET_FEN";

    static Map<String, String> commandURLMap = new HashMap<>();

    static {
        // new game
        String path = "http://chess-api-chess.herokuapp.com/api/v1/chess/two";
        commandURLMap.put(NEW_GAME, path);

        // list possible moves
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/two/moves";
        commandURLMap.put(LIST_POSSIBLE_MOVES, path);

        // move figure player
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/two/move";
        commandURLMap.put(MOVE_FIGURE_PLAYER, path);

        // check game over
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/two/check";
        commandURLMap.put(CHECK_GAME_OVER, path);

        // get turn
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/two/turn";
        commandURLMap.put(GET_TURN, path);

        // reset board
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/two/reset";
        commandURLMap.put(RESET_BOARD, path);

        // get fen
        path = "http://chess-api-chess.herokuapp.com/api/v1/chess/two/fen";
        commandURLMap.put(GET_FEN, path);

    }
}
