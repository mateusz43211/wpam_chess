package com.example.archess.wpam_chess.OnePlayer;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.CHECK_GAME_OVER;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.GET_TURN;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.LIST_POSSIBLE_MOVES;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.MOVE_FIGURE_AI;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.MOVE_FIGURE_PLAYER;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.NEW_GAME;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.PLAYER_HELP;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.RESET_BOARD;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.CHECK_GAME_OVER_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.ERROR;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.GET_TURN_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.LIST_POSSIBLE_MOVES_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.MOVE_FIGURE_AI_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.MOVE_FIGURE_PLAYER_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.NEW_GAME_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.PLAYER_HELP_FINISHED;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.ServiceStatus.RUNNING;
import static com.example.archess.wpam_chess.OnePlayer.ConstantsOnePlayer.UNDO_MOVE;

public class OnePlayerQueryService extends IntentService {

    public OnePlayerQueryService() {
        super("OnePlayerQueryService");
    }

    public OnePlayerQueryService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String command = intent.getStringExtra("command");
        Bundle b = new Bundle();
        if (command.equals(NEW_GAME)) {
            newGame(receiver, b);
        } else if (command.equals(LIST_POSSIBLE_MOVES)) {
            listPossibleMoves(intent, receiver, b);
        } else if (command.equals(MOVE_FIGURE_PLAYER)) {
            moveFigurePlayer(intent, receiver, b);
        } else if (command.equals(MOVE_FIGURE_AI)) {
            moveFigureAI(intent, receiver, b);
        } else if (command.equals(CHECK_GAME_OVER)) {
            checkGameOver(intent, receiver, b);
        } else if (command.equals(PLAYER_HELP)) {
            playerHelp(intent, receiver, b);
        } else if (command.equals(GET_TURN)) {
            getTurn(intent, receiver, b);
        } else if (command.equals(UNDO_MOVE)) {
            undoMove(intent, receiver, b);
        } else if (command.equals(RESET_BOARD)) {
            // resetBoard(intent, receiver, b);
        }
    }

    private void newGame(ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        try {
            JsonObject jsonObject = OnePlayerChessApiClient.getStartGame();
            if (jsonObject != null) {
                String game_id = "game_id";
                JsonElement element = jsonObject.get(game_id);
                b.putString(game_id, element.getAsString());
                receiver.send(NEW_GAME_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void listPossibleMoves(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String position = intent.getStringExtra("position");
        String game_id = intent.getStringExtra("game_id");

        try {
            JsonObject jsonObject = OnePlayerChessApiClient.getListPossibleMoves(position, game_id);
            if (jsonObject != null) {
                String moves = "moves";
                JsonElement element = jsonObject.get(moves);

                Type arrayListType = new TypeToken<ArrayList<String>>() {
                }.getType();
                ArrayList<String> possibleMoves = new Gson().fromJson(element, arrayListType);

                b.putStringArrayList(moves, possibleMoves);
                b.putString("position", position);
                receiver.send(LIST_POSSIBLE_MOVES_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void moveFigurePlayer(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String from = intent.getStringExtra("from");
        String to = intent.getStringExtra("to");
        String game_id = intent.getStringExtra("game_id");

        try {
            JsonObject jsonObject = OnePlayerChessApiClient.getMoveFigurePlayer(from, to, game_id);
            if (jsonObject != null) {
                String status = "status";
                JsonElement element = jsonObject.get(status);
                b.putString(status, element.getAsString());
                receiver.send(MOVE_FIGURE_PLAYER_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void moveFigureAI(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String game_id = intent.getStringExtra("game_id");
        try {
            JsonObject jsonObject = OnePlayerChessApiClient.getMoveFigureAI(game_id);
            if (jsonObject != null) {
                String from = "from";
                String to = "to";

                JsonElement elementFrom = jsonObject.get(from);
                JsonElement elementTo = jsonObject.get(to);

                b.putString(from, elementFrom.getAsString());
                b.putString(to, elementTo.getAsString());

                receiver.send(MOVE_FIGURE_AI_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void checkGameOver(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String game_id = intent.getStringExtra("game_id");
        try {
            JsonObject jsonObject = OnePlayerChessApiClient.getCheckGameOver(game_id);
            if (jsonObject != null) {
                String status = "status";

                JsonElement elementStatus = jsonObject.get(status);
                b.putString(status, elementStatus.getAsString());

                receiver.send(CHECK_GAME_OVER_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void playerHelp(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String game_id = intent.getStringExtra("game_id");
        try {
            JsonObject jsonObject = OnePlayerChessApiClient.getPlayerHelp(game_id);
            if (jsonObject != null) {
                String status = "status";
                String from = "from";
                String to = "to";

                JsonElement elementStatus = jsonObject.get(status);
                JsonElement elementFrom = jsonObject.get(from);
                JsonElement elementTo = jsonObject.get(to);

                b.putString(status, elementStatus.getAsString());
                b.putString(from, elementFrom.getAsString());
                b.putString(to, elementTo.getAsString());

                receiver.send(PLAYER_HELP_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void getTurn(Intent intent, ResultReceiver receiver, Bundle b) {
        receiver.send(RUNNING, Bundle.EMPTY);
        String game_id = intent.getStringExtra("game_id");
        try {
            JsonObject jsonObject = OnePlayerChessApiClient.getTurn(game_id);
            if (jsonObject != null) {
                String turn = "turn";

                JsonElement elementTurn = jsonObject.get(turn);
                b.putString(turn, elementTurn.getAsString());

                receiver.send(GET_TURN_FINISHED, b);
            }
        } catch (Exception e) {
            b.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(ERROR, b);
        }
    }

    private void undoMove(Intent intent, ResultReceiver receiver, Bundle b) {

    }
}
