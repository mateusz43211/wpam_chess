package com.example.archess.wpam_chess.AR;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.widget.Button;

import com.example.archess.wpam_chess.Logic.ChessFactory;
import com.example.archess.wpam_chess.Logic.IChessFigure;
import com.example.archess.wpam_chess.Logic.IGameManager;
import com.example.archess.wpam_chess.Logic.ITile;
import com.example.archess.wpam_chess.OnGameListener;
import com.example.archess.wpam_chess.R;
import com.google.ar.core.AugmentedImage;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.collision.Box;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.Material;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ShapeFactory;
import com.google.ar.sceneform.rendering.ViewRenderable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.archess.wpam_chess.AR.ConstantsAR.PAWN_SCALE_FACTOR;
import static com.example.archess.wpam_chess.AR.ConstantsAR.SCALE_FACTOR;

class AugmentedImageNode extends AnchorNode implements OnGameListener, OnLoadingFinishedListener {
    private static final String TAG = "AugmentedImageNode";
    private final Context context;

    private Vector3 modelScaleVector = new Vector3(SCALE_FACTOR, SCALE_FACTOR, SCALE_FACTOR);
    private Vector3 pawnScaleVector = new Vector3(PAWN_SCALE_FACTOR, PAWN_SCALE_FACTOR, PAWN_SCALE_FACTOR);

    // The augmented image represented by this node.
    private AugmentedImage image;

    private final int BOARD_SIZE = 8;
    private float tileSize;

    private ModelsFactory modelsFactory;
    private BoardNode boardNode;
    private Vector3 boardSize = new Vector3();
    private IGameManager gameManager;
    private Vector3 buttonPosition = Vector3.zero();
    private final Map<String, Vector3> positionMap = new HashMap<>();
    private Node playerTurnNode;


    AugmentedImageNode(Context context, AugmentedImage image, IGameManager gameManager) {
        super();
        this.image = image;
        this.gameManager = gameManager;
        this.context = context;
        modelsFactory = new ModelsFactory(context, this);

        // Set the anchor based on the center of the image.
        setAnchor(image.createAnchor(image.getCenterPose()));

    }

    @Override
    public void onLoadingFinished() {
        Quaternion localRotation = gameManager.getPlayerColor().equals("w") ?
                Quaternion.rotationBetweenVectors(Vector3.up(), Vector3.forward()) :
                Quaternion.multiply(Quaternion.rotationBetweenVectors(Vector3.up(), Vector3.forward()),
                                    Quaternion.axisAngle(Vector3.forward(), 180f));


        boardNode = new BoardNode(gameManager);
        boardNode.setLocalScale(modelScaleVector);
        boardNode.setRenderable(modelsFactory.getBoard());
        boardNode.setParent(this);
        boardNode.setLocalRotation(localRotation);

        Box boardBox = (Box) boardNode.getCollisionShape();
        boardSize.set(boardBox.getSize());

        float zOffset = gameManager.getPlayerColor().equals("w") ? boardSize.x / 2f : -boardSize.x / 2f;

        boardNode.setLocalPosition(new Vector3(0f, 0f, zOffset).scaled(SCALE_FACTOR));

        tileSize = getTileSize(boardBox);

        loadTilesPositions();
        generateFigures();

        Material possibleMoveMaterial = modelsFactory.getPossibleMoveMaterial();
        Material occupiedMaterial = modelsFactory.getOccupiedMaterial();
        Material occupiedByOpponentMaterial = modelsFactory.getOccupiedByOpponentMaterial();

        float factor = 0.8f;
        Vector3 modelSize = new Vector3(tileSize * factor, tileSize * factor, 0.01f);
        Vector3 currentSize = new Vector3(tileSize, tileSize, 0.01f);
        ModelRenderable possibleMoveModel =
                ShapeFactory.makeCube(modelSize, Vector3.zero(), possibleMoveMaterial);

        ModelRenderable occupiedModel =
                ShapeFactory.makeCube(currentSize, Vector3.zero(), occupiedMaterial);

        ModelRenderable occupiedByOpponentModel =
                ShapeFactory.makeCube(modelSize, Vector3.zero(), occupiedByOpponentMaterial);

        Material hintMaterial = modelsFactory.getPossibleMoveMaterial();
        hintMaterial.setFloat3(MaterialFactory.MATERIAL_COLOR, new Color(android.graphics.Color.GREEN));

        ModelRenderable hintModel = ShapeFactory.makeCube(modelSize, Vector3.zero(), hintMaterial);

        for (Map.Entry<String, Vector3> entry : positionMap.entrySet()) {
            ITile tile = gameManager.getTile(entry.getKey());
            TileNode node = new TileNode(tile, possibleMoveModel.makeCopy(),
                                         occupiedModel.makeCopy(),
                                         occupiedByOpponentModel.makeCopy(),
                                         hintModel.makeCopy());
            node.setLocalPosition(entry.getValue());
            node.setName(entry.getKey());
            node.setParent(boardNode);
        }

        boardNode.generateOutPositions(tileSize, possibleMoveModel);

        Vector3 tempSize = new Vector3(40f, 40f, 0.01f);
        ModelRenderable renderable = ShapeFactory.makeCube(tempSize, Vector3.zero(), hintMaterial);

        playerTurnNode = new Node();
        playerTurnNode.setRenderable(renderable);
        playerTurnNode.setLocalPosition(new Vector3(0, tempSize.y / 2f - 4f, -boardSize.z / 2f - 0.01f));
        playerTurnNode.setName("playerTurn");
        boardNode.addChild(playerTurnNode);
        changeTurnNode(gameManager.isPlayerTurn());

        buttonPosition.set(0f, -300f, boardSize.x * 2);

        if (!gameManager.isTwoPlayerGame()) {
            ViewRenderable.builder()
                    .setView(context, R.layout.hint_button)
                    .build()
                    .thenAccept(viewRenderable -> {
                        Node node = new Node();
                        node.setRenderable(viewRenderable);
                        node.setLocalPosition(buttonPosition.scaled(SCALE_FACTOR));
                        node.setParent(boardNode);

                        Button button = (Button) viewRenderable.getView();
                        button.setOnClickListener(v -> {
                            gameManager.getHint();
                        });


                    });
        }
    }

    private void loadTilesPositions() {
        String tempKey;
        Vector3 position;
        float y, x;
        float offset = tileSize * 0.5f;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {

                tempKey = ChessFactory.getPosition(j + 1, i + 1);
                Log.d(TAG, "tempKey: " + tempKey);
                x = (i - 4) * tileSize + offset;
                y = (j - 4) * tileSize + offset + boardSize.y / 2f;

                position = new Vector3(x, y, boardSize.z / 2f);
                positionMap.put(tempKey, position);
            }
        }
    }

    private void generateFigures() {
        ArrayList<IChessFigure> figures = gameManager.getAllFigures();

        for (IChessFigure fig : figures) {
            FigureNode figureNode = new FigureNode(fig, gameManager);
            figureNode.setParent(boardNode);
            figureNode.setRenderable(modelsFactory.getFigureModel(fig.getType()));
            figureNode.setLocalScale(pawnScaleVector);
            figureNode.setName(fig.getName());

            Vector3 localPosition = getTilePosition(fig.getPosition());
            Box box = (Box) figureNode.getCollisionShape();
            Vector3 offset = new Vector3(0f, -box.getExtents().y, box.getExtents().z).scaled(PAWN_SCALE_FACTOR);
            figureNode.setLocalPosition(Vector3.add(localPosition, offset));
        }
    }

    private float getTileSize(Box box) {
        Vector3 size = box.getSize();
        return size.x / 9.4f;
    }

    private Vector3 getTilePosition(String positionSymbol) {
        Vector3 pos = positionMap.get(positionSymbol);
        return pos != null ? new Vector3(pos) : Vector3.zero();
    }

    public AugmentedImage getImage() {
        return image;
    }

    @Override
    public void OnMoveAI(String from, String to) {
        IChessFigure figure = gameManager.getFigureByPosition(from);
        if (figure != null) {
            boardNode.moveAiFigure(figure, to);
            changeTurnNode(true);
        }
    }

    private void changeTurnNode(boolean forPlayer) {
        Material m = playerTurnNode.getRenderable().getMaterial();
        if (forPlayer) {
            m.setFloat3(MaterialFactory.MATERIAL_COLOR, new Color(android.graphics.Color.GREEN));
        } else {
            m.setFloat3(MaterialFactory.MATERIAL_COLOR, new Color(android.graphics.Color.RED));
        }
    }


    @Override
    public void onMoveOutFigure(String figureName) {
        boardNode.moveOutFigure(figureName);
    }

    @Override
    public void showHint(String from, String to) {
        boardNode.showHint(from, to);
    }

    @Override
    public void onGameOver(String winner) {
        String message;

        if (winner.equals(gameManager.getPlayerColor())) {
            message = "Congratulations! You win";
        } else {
            message = "You lose";
        }
        new AlertDialog.Builder(context)
                .setTitle("Check mate")
                .setMessage(message)

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setNeutralButton("Ok", (dialog, which) -> {
                    Activity activity = (Activity)context;
                    activity.finish();
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onMovePlayer(boolean isPlayerMove) {
        changeTurnNode(isPlayerMove);
    }
}
