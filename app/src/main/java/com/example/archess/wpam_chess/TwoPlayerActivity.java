package com.example.archess.wpam_chess;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.example.archess.wpam_chess.TwoPlayers.HostChooseActivity;
import com.example.archess.wpam_chess.TwoPlayers.JoinGameActivity;

public class TwoPlayerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player);

        Button hostButton = findViewById(R.id.hostButton);
        hostButton.setOnClickListener(v -> {
            Intent intent = new Intent(TwoPlayerActivity.this, HostChooseActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        });

        Button joinButton = findViewById(R.id.joinButton);
        joinButton.setOnClickListener(v -> {
            Intent intent = new Intent(TwoPlayerActivity.this, JoinGameActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        });
    }
}
