package com.example.archess.wpam_chess.TwoPlayers;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.example.archess.wpam_chess.R;

public class HostChooseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host_choose);
        Button createRoomButton = findViewById(R.id.createRoomButton);
        createRoomButton.setOnClickListener(v -> {
            Intent intent = new Intent(HostChooseActivity.this, HostGameActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        });

        Button joinRoomButton = findViewById(R.id.joinRoomButton);
        joinRoomButton.setOnClickListener(v -> {
            Intent intent = new Intent(HostChooseActivity.this, JoinGameActivity.class);
            intent.putExtra("color", "w");
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        });

    }
}
