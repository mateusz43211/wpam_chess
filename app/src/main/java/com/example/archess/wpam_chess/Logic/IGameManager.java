package com.example.archess.wpam_chess.Logic;

import java.util.ArrayList;

public interface IGameManager {
    void movePlayer(String from, String to);
    IChessFigure getFigureByPosition(String position);
    IChessFigure getFigureByName(String name);
    ArrayList<IChessFigure> getAllFigures();
    String getPlayerColor();
    String getPlayerTurn();
    boolean isPlayerTurn();
    ITile getTile(String position);
    void getHint();

    boolean isTwoPlayerGame();
}
