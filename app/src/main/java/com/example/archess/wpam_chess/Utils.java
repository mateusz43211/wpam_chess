package com.example.archess.wpam_chess;

import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class Utils {
    private static final int RESULT_OK_CODE = 200;
    private static final int RESULT_NOT_FOUND = 404;

    public static JsonObject getResponseJsonObject(HttpURLConnection connection) {
        try {
            JsonObject jsonObject;
            int responseCode = connection.getResponseCode();
            if (responseCode == RESULT_OK_CODE) {
                InputStream inputStream = connection.getInputStream();
                JsonParser parser = new JsonParser();
                Object obj = parser.parse(new InputStreamReader(inputStream));
                jsonObject = (JsonObject) obj;
                return jsonObject;
            } else if (responseCode == RESULT_NOT_FOUND) {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void showToastWithMessage(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, 100);
        TextView v = toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
        }
        toast.show();
    }
}
