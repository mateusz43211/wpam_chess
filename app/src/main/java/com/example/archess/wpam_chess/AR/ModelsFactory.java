package com.example.archess.wpam_chess.AR;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.google.ar.sceneform.rendering.Color;
import com.google.ar.sceneform.rendering.Material;
import com.google.ar.sceneform.rendering.MaterialFactory;
import com.google.ar.sceneform.rendering.ModelRenderable;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

class ModelsFactory {
    private static final String TAG = "ModelsFactory";
    private ModelRenderable board;
    private Map<String, ModelRenderable> figureModelMap = new HashMap<>();
    private Material possibleMoveMaterial, occupiedMaterial, occupiedByOpponentMaterial;

    ModelsFactory(Context context, OnLoadingFinishedListener listener) {
        // build all models
        CompletableFuture<ModelRenderable> boardStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("board.sfb")).build();

        // whites
        CompletableFuture<ModelRenderable> wKingStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("w_king.sfb")).build();
        CompletableFuture<ModelRenderable> wQueenStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("w_queen.sfb")).build();
        CompletableFuture<ModelRenderable> wRookStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("w_rook.sfb")).build();
        CompletableFuture<ModelRenderable> wBishopStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("w_bishop.sfb")).build();
        CompletableFuture<ModelRenderable> wKnightStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("w_knight.sfb")).build();
        CompletableFuture<ModelRenderable> wPawnStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("w_pawn.sfb")).build();

        // blacks
        CompletableFuture<ModelRenderable> bKingStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("figures/b_king.sfb")).build();
        CompletableFuture<ModelRenderable> bQueenStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("figures/b_queen.sfb")).build();
        CompletableFuture<ModelRenderable> bRookStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("figures/b_rook.sfb")).build();
        CompletableFuture<ModelRenderable> bBishopStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("figures/b_bishop.sfb")).build();
        CompletableFuture<ModelRenderable> bKnightStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("figures/b_knight.sfb")).build();
        CompletableFuture<ModelRenderable> bPawnStage = ModelRenderable.builder()
                .setSource(context, Uri.parse("figures/b_pawn.sfb")).build();

        // materials

        Color currentFigureColor = new Color(1f, 1f, 0f, 1f);
        Color possibleMoveColor = new Color(0.2f, 0.8f, 1f, 0.95f);
        Color occupiedByOpponentColor = new Color(1f, 0f, 0f, 0.95f);
        CompletableFuture<Material> possibleMoveMaterialStage =
                MaterialFactory.makeOpaqueWithColor(context, possibleMoveColor);
        CompletableFuture<Material> occupiedMaterialStage =
                MaterialFactory.makeOpaqueWithColor(context, currentFigureColor);
        CompletableFuture<Material> occupiedByOpponentMaterialStage =
                MaterialFactory.makeOpaqueWithColor(context, occupiedByOpponentColor);

        CompletableFuture.allOf(
                boardStage,
                wKingStage,
                wQueenStage,
                wRookStage,
                wBishopStage,
                wKnightStage,
                wPawnStage,
                bKingStage,
                bQueenStage,
                bRookStage,
                bBishopStage,
                bKnightStage,
                bPawnStage,
                possibleMoveMaterialStage,
                occupiedMaterialStage,
                occupiedByOpponentMaterialStage
        ).handle((notUsed, throwable) -> {
            if (throwable != null) {
                Log.e(TAG, "Unable to load renderable");
                return null;
            }

            try {
                board = boardStage.get();

                figureModelMap.put("K", wKingStage.get());
                figureModelMap.put("Q", wQueenStage.get());
                figureModelMap.put("R", wRookStage.get());
                figureModelMap.put("B", wBishopStage.get());
                figureModelMap.put("N", wKnightStage.get());
                figureModelMap.put("P", wPawnStage.get());

                figureModelMap.put("k", bKingStage.get());
                figureModelMap.put("q", bQueenStage.get());
                figureModelMap.put("r", bRookStage.get());
                figureModelMap.put("b", bBishopStage.get());
                figureModelMap.put("n", bKnightStage.get());
                figureModelMap.put("p", bPawnStage.get());

                possibleMoveMaterial = possibleMoveMaterialStage.get();
                occupiedMaterial = occupiedMaterialStage.get();
                occupiedByOpponentMaterial = occupiedByOpponentMaterialStage.get();

                // Everything finished loading successfully.
                listener.onLoadingFinished();

            } catch (InterruptedException | ExecutionException ex) {
                Log.e(TAG, "Unable to load renderable");
            }

            return null;
        });
    }

    ModelRenderable getBoard() {
        return board.makeCopy();
    }

    Material getPossibleMoveMaterial() {
        return possibleMoveMaterial.makeCopy();
    }

    Material getOccupiedMaterial() {
        return occupiedMaterial.makeCopy();
    }

    Material getOccupiedByOpponentMaterial() {
        return occupiedByOpponentMaterial.makeCopy();
    }

    ModelRenderable getFigureModel(String figureType) {
        return figureModelMap.get(figureType).makeCopy();
    }
}
