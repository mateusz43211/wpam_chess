package com.example.archess.wpam_chess;

public interface OnGameListener {
    void OnMoveAI(String from, String to);
    void onMoveOutFigure(String figureName);
    void showHint(String from, String to);
    void onGameOver(String winner);

    void onMovePlayer(boolean isPlayerMove);
}
