package com.example.archess.wpam_chess.AR;

class ConstantsAR {
    static final float PAWN_SCALE_FACTOR = 2f;
    static final float SCALE_FACTOR = 0.015f;
}
