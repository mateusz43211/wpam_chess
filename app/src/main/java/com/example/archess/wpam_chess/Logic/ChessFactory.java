package com.example.archess.wpam_chess.Logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChessFactory {

    private static String rows[] = new String[]{"1", "2", "3", "4", "5", "6", "7", "8"};
    private static String columns[] = new String[]{"a", "b", "c", "d", "e", "f", "g", "h"};
    private static int currentFigureID = 0;

    static IChessFigure createFigure(char figureSymbol, int row, int col) {
        String type = Character.toString(figureSymbol);
        String color = getColor(figureSymbol);
        String position = getPosition(row, col);
        String name = String.valueOf(currentFigureID);
        currentFigureID++;
        return new ChessFigure(type, color, position, name);
    }

    public static String getPosition(int row, int col) {
        return (columns[col - 1] + rows[row - 1]);
    }

    private static String getColor(char figureSymbol) {
        if (Character.isUpperCase(figureSymbol)) {
            return "w";
        } else {
            return "b";
        }
    }

    static Map<String, ITile> creteChessboard() {
        Map<String, ITile> tileMap = new HashMap<>();

        for (int r = 1; r <= 8; ++r) {
            for (int c = 1; c <= 8; ++c) {
                String position = getPosition(r, c);
                ITile tile = new Tile(position);
                tileMap.put(tile.getPosition(), tile);
            }
        }
        return tileMap;
    }

    public static ArrayList<String> getAllChessboardPositions() {
        ArrayList<String> positions = new ArrayList<>();
        for (int r = 1; r <= 8; ++r) {
            for (int c = 1; c <= 8; ++c) {
                String pos = getPosition(r, c);
                positions.add(pos);
            }
        }
        return positions;
    }
}
