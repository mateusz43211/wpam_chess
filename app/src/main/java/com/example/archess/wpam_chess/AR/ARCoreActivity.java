package com.example.archess.wpam_chess.AR;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.archess.wpam_chess.CameraPermissionHelper;
import com.example.archess.wpam_chess.Logic.GameManager;
import com.example.archess.wpam_chess.OnePlayer.GameManagerOnePlayer;
import com.example.archess.wpam_chess.R;
import com.example.archess.wpam_chess.SnackbarHelper;
import com.example.archess.wpam_chess.TwoPlayers.GameManagerTwoPlayers;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.AugmentedImageDatabase;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.ux.ArFragment;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public class ARCoreActivity extends AppCompatActivity {
    private static final String TAG = "ARCoreActivity";
    private static final String DEFAULT_IMAGE_DATABASE_NAME = "20052019_qrcode.imgdb";

    Session mSession;
    private boolean mUserRequestedInstall = true;
    ArFragment arFragment;
    private ImageView fitToScanView;
    private GameManager gameManager;
    private Map<AugmentedImage, AugmentedImageNode> augmentedImageMap = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arcore);
        fitToScanView = findViewById(R.id.image_view_fit_to_scan);

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);
        arFragment.getArSceneView().getScene().addOnUpdateListener(this::onUpdateFrame);
        arFragment.getPlaneDiscoveryController().hide();
        arFragment.getPlaneDiscoveryController().setInstructionView(null);
        arFragment.getArSceneView().getPlaneRenderer().setEnabled(false);

        String numberOfPlayers = getIntent().getStringExtra("number_of_players");

        if (numberOfPlayers.equals("one")){
            gameManager = new GameManagerOnePlayer(this);
        }
        else if (numberOfPlayers.equals("two")){
            String gameId = getIntent().getStringExtra("game_id");

            String color = getIntent().getStringExtra("color");

            gameManager = new GameManagerTwoPlayers(this, gameId, color);
        }


        gameManager.start();

    }

    private void onUpdateFrame(FrameTime frameTime) {

        Frame frame = arFragment.getArSceneView().getArFrame();
        if (frame == null || frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
            return;
        }

        Collection<AugmentedImage> updatedAugmentedImages =
                frame.getUpdatedTrackables(AugmentedImage.class);

        for (AugmentedImage augmentedImage : updatedAugmentedImages) {
            switch (augmentedImage.getTrackingState()) {
                case PAUSED:
                    // When an image is in PAUSED state, but the camera is not PAUSED, it has been detected,
                    // but not yet tracked.
                    String text = "Detected Image " + augmentedImage.getIndex();
                    SnackbarHelper.getInstance().showMessage(this, text);
                    break;

                case TRACKING:
                    // Have to switch to UI Thread to update View.
                    if (fitToScanView.getVisibility() == View.VISIBLE){
                        fitToScanView.setVisibility(View.GONE);
                    }

                    // Create a new anchor for newly found images.
                    if (!augmentedImageMap.containsKey(augmentedImage)) {
                        AugmentedImageNode node = new AugmentedImageNode(this, augmentedImage,
                                                                         gameManager);
                        augmentedImageMap.put(augmentedImage, node);
                        arFragment.getArSceneView().getScene().addChild(node);
                        gameManager.setOnGameListener(node);


                    }
                    break;

                case STOPPED:
                    augmentedImageMap.remove(augmentedImage);
                    break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameManager.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // ARCore requires camera permission to operate.
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            CameraPermissionHelper.requestCameraPermission(this);
            return;
        }

        if (augmentedImageMap.isEmpty()) {
            fitToScanView.setVisibility(View.VISIBLE);
        }

        gameManager.onResume();

        try {
            if (mSession == null) {
                switch (ArCoreApk.getInstance().requestInstall(this, mUserRequestedInstall)) {
                    case INSTALLED:
                        // Success, create the AR session.
                        mSession = new Session(this);
                        Config config = new Config(mSession);

                        try {
                            InputStream inputStream = getAssets().open(DEFAULT_IMAGE_DATABASE_NAME);
                            AugmentedImageDatabase imageDatabase = AugmentedImageDatabase.deserialize(mSession, inputStream);
                            config.setAugmentedImageDatabase(imageDatabase);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        config.setPlaneFindingMode(Config.PlaneFindingMode.DISABLED);
                        config.setUpdateMode(Config.UpdateMode.LATEST_CAMERA_IMAGE);
                        mSession.configure(config);

                        arFragment.getArSceneView().setupSession(mSession);

                        break;
                    case INSTALL_REQUESTED:
                        // Ensures next invocation of requestInstall() will either return
                        // INSTALLED or throw an exception.
                        mUserRequestedInstall = false;
                        return;
                }
            }
        } catch (UnavailableUserDeclinedInstallationException e) {
            // Display an appropriate message to the user and return gracefully.
            Toast.makeText(this, "TODO: handle exception " + e, Toast.LENGTH_LONG)
                    .show();
            return;
        } catch (UnavailableArcoreNotInstalledException | UnavailableDeviceNotCompatibleException | UnavailableSdkTooOldException | UnavailableApkTooOldException e) {
            e.printStackTrace();
        }

        if (augmentedImageMap.isEmpty()) {
            fitToScanView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            Toast.makeText(this, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
                    .show();
            if (!CameraPermissionHelper.shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                CameraPermissionHelper.launchPermissionSettings(this);
            }
            finish();
        }
    }
}
