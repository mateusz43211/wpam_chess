package com.example.archess.wpam_chess.Logic;

import java.util.ArrayList;

public class MoveValidator {
    public static ArrayList<String> getMoveFromStatus(String beforeFEN, String afterFEN) {
        // index 0 - move from
        // index 1 - move to
        String moveFrom = null, moveTo = null;

        int space_index = beforeFEN.indexOf(" ");
        String positions = beforeFEN.substring(0, space_index);
        String rows[] = positions.split("/");

        char startChessboard[][] = chessboardFromFEN(beforeFEN);
        char endChessboard[][] = chessboardFromFEN(afterFEN);

        ArrayList<int[]> changes = getChanges(startChessboard, endChessboard);

        ArrayList<String> result = new ArrayList<>();

        if (changes.size() == 2) {
            // simple move
            char start1 = startChessboard[changes.get(0)[0]][changes.get(0)[1]];
            char start2 = startChessboard[changes.get(1)[0]][changes.get(1)[1]];
            char end1 = endChessboard[changes.get(0)[0]][changes.get(0)[1]];
            char end2 = endChessboard[changes.get(1)[0]][changes.get(1)[1]];


            if (start1 != '0' && end1 == '0') {
                moveFrom = ChessFactory.getPosition(changes.get(0)[0] + 1, changes.get(0)[1] + 1);
                moveTo = ChessFactory.getPosition(changes.get(1)[0] + 1, changes.get(1)[1] + 1);
            } else if (start2 != '0' && end2 == '0') {
                moveTo = ChessFactory.getPosition(changes.get(0)[0] + 1, changes.get(0)[1] + 1);
                moveFrom = ChessFactory.getPosition(changes.get(1)[0] + 1, changes.get(1)[1] + 1);
            }


        } else if (changes.size() == 4) {
            // castling
            char s1 = startChessboard[changes.get(0)[0]][changes.get(0)[1]];
            char s2 = startChessboard[changes.get(1)[0]][changes.get(1)[1]];
            char s3 = startChessboard[changes.get(2)[0]][changes.get(2)[1]];
            char s4 = startChessboard[changes.get(3)[0]][changes.get(3)[1]];

            char e1 = endChessboard[changes.get(0)[0]][changes.get(0)[1]];
            char e2 = endChessboard[changes.get(1)[0]][changes.get(1)[1]];
            char e3 = endChessboard[changes.get(2)[0]][changes.get(2)[1]];
            char e4 = endChessboard[changes.get(3)[0]][changes.get(3)[1]];

            // check king
            if (s1 == '0' && isFigureKing(e1)) {
                moveTo = ChessFactory.getPosition(changes.get(0)[0] + 1, changes.get(0)[1] + 1);
            } else if (s2 == '0' && isFigureKing(e2)) {
                moveTo = ChessFactory.getPosition(changes.get(1)[0] + 1, changes.get(1)[1] + 1);
            } else if (s3 == '0' && isFigureKing(e3)) {
                moveTo = ChessFactory.getPosition(changes.get(2)[0] + 1, changes.get(2)[1] + 1);
            } else if (s4 == '0' && isFigureKing(e4)) {
                moveTo = ChessFactory.getPosition(changes.get(3)[0] + 1, changes.get(3)[1] + 1);
            }

            if (e1 == '0' && isFigureKing(s1)) {
                moveFrom = ChessFactory.getPosition(changes.get(0)[0] + 1, changes.get(0)[1] + 1);
            } else if (e2 == '0' && isFigureKing(s2)) {
                moveFrom = ChessFactory.getPosition(changes.get(1)[0] + 1, changes.get(1)[1] + 1);
            } else if (e3 == '0' && isFigureKing(s3)) {
                moveFrom = ChessFactory.getPosition(changes.get(2)[0] + 1, changes.get(2)[1] + 1);
            } else if (e4 == '0' && isFigureKing(s4)) {
                moveFrom = ChessFactory.getPosition(changes.get(3)[0] + 1, changes.get(3)[1] + 1);
            }

            result.add(moveFrom);
            result.add(moveTo);

            if (s1 == '0' && isFigureRook(e1)) {
                moveTo = ChessFactory.getPosition(changes.get(0)[0] + 1, changes.get(0)[1] + 1);
            } else if (s2 == '0' && isFigureRook(e2)) {
                moveTo = ChessFactory.getPosition(changes.get(1)[0] + 1, changes.get(1)[1] + 1);
            } else if (s3 == '0' && isFigureRook(e3)) {
                moveTo = ChessFactory.getPosition(changes.get(2)[0] + 1, changes.get(2)[1] + 1);
            } else if (s4 == '0' && isFigureRook(e4)) {
                moveTo = ChessFactory.getPosition(changes.get(3)[0] + 1, changes.get(3)[1] + 1);
            }

            if (e1 == '0' && isFigureRook(s1)) {
                moveFrom = ChessFactory.getPosition(changes.get(0)[0] + 1, changes.get(0)[1] + 1);
            } else if (e2 == '0' && isFigureRook(s2)) {
                moveFrom = ChessFactory.getPosition(changes.get(1)[0] + 1, changes.get(1)[1] + 1);
            } else if (e3 == '0' && isFigureRook(s3)) {
                moveFrom = ChessFactory.getPosition(changes.get(2)[0] + 1, changes.get(2)[1] + 1);
            } else if (e4 == '0' && isFigureRook(s4)) {
                moveFrom = ChessFactory.getPosition(changes.get(3)[0] + 1, changes.get(3)[1] + 1);
            }
            result.add(moveFrom);
            result.add(moveTo);
        }


        return result;
    }

    private static boolean isFigureKing(char figure) {
        return figure == 'K' || figure == 'k';
    }

    private static boolean isFigureRook(char figure) {
        return figure == 'K' || figure == 'k';
    }

    private static ArrayList<int[]> getChanges(char[][] startChessboard, char[][] lastChessboard) {
        ArrayList<int[]> result = new ArrayList<>();
        for (int r = 0; r < 8; ++r) {
            for (int c = 0; c < 8; ++c) {
                if (startChessboard[r][c] != lastChessboard[r][c]) {
                    result.add(new int[]{r, c});
                }
            }
        }

        return result;
    }

    private static int getRowForDigit(int value) {
        switch (value) {
            case 1:
                return 8;
            case 2:
                return 7;
            case 3:
                return 6;
            case 4:
                return 5;
            case 5:
                return 4;
            case 6:
                return 3;
            case 7:
                return 2;
            case 8:
                return 1;
        }
        return 1;
    }

    private static char[][] chessboardFromFEN(String fen) {
        int space_index = fen.indexOf(" ");
        String positions = fen.substring(0, space_index);
        String rows[] = positions.split("/");

        char result[][] = new char[8][8];
        for (int r = 0; r < 8; ++r) {
            for (int c = 0; c < 8; ++c) {
                result[r][c] = '0';
            }
        }

        for (int r = rows.length; r > 0; --r) {
            String current_row = rows[r - 1];
            int column_index = 1;
            for (int c = 0; c < current_row.length(); c++) {
                char current_char = current_row.charAt(c);
                if (Character.isDigit(current_char)) {
                    // this is a gap
                    column_index += Character.getNumericValue(current_char);
                } else {
                    // this is a figure
                    result[getRowForDigit(r) - 1][column_index - 1] = current_char;
                    column_index++;
                }
            }
        }

        return result;
    }
}
