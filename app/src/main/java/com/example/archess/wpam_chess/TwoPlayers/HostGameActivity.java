package com.example.archess.wpam_chess.TwoPlayers;

import android.app.ActivityOptions;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.archess.wpam_chess.AR.ARCoreActivity;
import com.example.archess.wpam_chess.MyResultReceiver;
import com.example.archess.wpam_chess.R;
import com.example.archess.wpam_chess.Utils;

import static com.example.archess.wpam_chess.TwoPlayers.ConstantsTwoPlayer.NEW_GAME;

public class HostGameActivity extends AppCompatActivity implements MyResultReceiver.Receiver {

    private MyResultReceiver myResultReceiver;
    private Button generateIdButton, startHostButton, sendButton;
    private ImageButton copyToClipboardButton;
    private TextView gameIdTitleTextView, gameIdTextView, copyTextView;
    private String GAME_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_host_game);

        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(this);

        generateIdButton = findViewById(R.id.generateIdButton);
        generateIdButton.setOnClickListener(v -> {
            createNewGame();
        });

        startHostButton = findViewById(R.id.startHostGame);
        startHostButton.setOnClickListener(v -> {
            // start game
            Intent intent = new Intent(HostGameActivity.this, ARCoreActivity.class);
            intent.putExtra("game_id", GAME_ID);
            intent.putExtra("number_of_players", "two");
            intent.putExtra("color", "w");
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        });

        sendButton = findViewById(R.id.sendButton);
        sendButton.setOnClickListener(v -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent
                    .putExtra(Intent.EXTRA_TEXT,
                              GAME_ID);
            sendIntent.setType("text/plain");
//            sendIntent.setPackage("com.facebook.orca");
            try {
                startActivity(sendIntent);
            }
            catch (android.content.ActivityNotFoundException ex) {
                Utils.showToastWithMessage(this,"Please Install Facebook Messenger" );
            }
        });

        copyToClipboardButton = findViewById(R.id.copyToClipboardButton);
        copyToClipboardButton.setOnClickListener(v -> {
            String game_id = gameIdTextView.getText().toString();
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("game_id", game_id);
            clipboard.setPrimaryClip(clip);
            Utils.showToastWithMessage(this, "Game ID copied to clipboard");
        });

        gameIdTitleTextView = findViewById(R.id.gameIdTitleTextView);

        gameIdTextView = findViewById(R.id.gameIdTextView);

        copyTextView = findViewById(R.id.copyTextView);
    }

    private void createNewGame() {
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, getApplicationContext(), TwoPlayersQueryService.class);
        intent.putExtra("receiver", myResultReceiver);
        intent.putExtra("command", NEW_GAME);
        startService(intent);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case ConstantsTwoPlayer.ServiceStatus.RUNNING:
                break;

            case ConstantsTwoPlayer.ServiceStatus.NEW_GAME_FINISHED:
                String game_id = resultData.getString("game_id");
                String status = resultData.getString("status");
                if (status != null && status.equals("new game started")){
                    // started game

                    gameIdTitleTextView.setVisibility(View.VISIBLE);

                    gameIdTextView.setVisibility(View.VISIBLE);
                    gameIdTextView.setText(game_id);

                    copyToClipboardButton.setVisibility(View.VISIBLE);

                    copyTextView.setVisibility(View.VISIBLE);

                    startHostButton.setEnabled(true);
                    startHostButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.design_default_color_primary));

                    sendButton.setEnabled(true);
                    sendButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.design_default_color_primary));

                    GAME_ID = game_id;
                }
                else {
                    // error
                    Utils.showToastWithMessage(this, "The game has expired");
                }


                break;
            case ConstantsTwoPlayer.ServiceStatus.ERROR:
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        myResultReceiver.setReceiver(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        myResultReceiver.setReceiver(null);
    }
}
