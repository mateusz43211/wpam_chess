package com.example.archess.wpam_chess.Logic;

import com.example.archess.wpam_chess.Logic.IChessFigure;
import com.example.archess.wpam_chess.Logic.ITile;

public class Tile implements ITile {
    private final String position;
    private IChessFigure figure;

    Tile(String position) {
        this.position = position;
    }

    @Override
    public IChessFigure getFigure() {
        return figure;
    }

    @Override
    public void setFigure(IChessFigure figure) {
        this.figure = figure;
    }

    @Override
    public boolean isOccupied() {
        return figure != null;
    }

    @Override
    public String getPosition() {
        return position;
    }



}
