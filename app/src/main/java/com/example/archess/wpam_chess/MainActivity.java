package com.example.archess.wpam_chess;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.example.archess.wpam_chess.AR.ARCoreActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button button = findViewById(R.id.onePlayerButton);
        button.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, ARCoreActivity.class);
            intent.putExtra("number_of_players", "one");
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        });

        button = findViewById(R.id.twoPlayersButton);
        button.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, TwoPlayerActivity.class);
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
